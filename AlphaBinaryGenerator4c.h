#ifndef _ALPHABINARYGENERATOR4C_H_
#define _ALPHABINARYGENERATOR4C_H_

/*
Alpha-Binary Generator library is sequence binary generator library using Alphabet generation algorithm.
It generates all combinations of the binary up to number 'N' efficiently with immense speed.

it achieves 4x times speed than normal binary generator ,
it uses buffer to store 4-binary numbers and writes them at once to STDOUT or to FILE,
thus achieving maximum throughput.

Algorithm INFO : 
Time complexity of this algorithm is O(n^2) , but it still manages to generate binary numbers 
faster than algorithm having time complexity of O(n).

Output Correction : 
Since this algorithm generates binary numbers in buffer which contains binary of 4 numbers at once
so output could be 4x times less then or greater than actual value i.e 'N'.


The best way to test this program is to output to /dev/null, otherwise 
the file I/O will dominate the test time.

Alphabet generation algorithm by Jsdemonsim could be found here.
https://github.com/Jsdemonsim/Stackoverflow/blob/master/alphabet/alphabet.c

/*Version V 1.0.0

Summary of methods included.

1)Generate binary to STDOUT.
2)Generate binary to FILE.

Written by HaseeB Mir (haseebmir.hm@gmail.com)
Dated : 05/12/2017
*/

#include<stdio.h> /*For I/O Operations */
#include<stdlib.h> /*For For malloc() ,exit() */
#include<string.h>	/*for strlen() */
#include<stdint.h> /*For standard data types */
#include<math.h>  /*For log2l() & floor() */
#include<time.h> /*For clock generation */
#include<stdbool.h> /*For booleans*/
#include<unistd.h> /*For IO operation * (POSIX)*/

#define BYTE  8	  /*Defining single byte*/
#define WORD  16 /*Defining single word*/
#define DWORD 32 /*Defining double word*/
#define QWORD 64 /*Defining quad word*/

#define OUT_STDOUT 1 /*Defining ID for STDOUT*/
#define OUT_FILE 2 /*Defining ID for FILE*/

/****************************************************************************/
/*********************-PUBLIC-METHODS-***************************************/
/****************************************************************************/

/*Public methods to generate binary representation to STDOUT or FILE*/
void generateBinary(uint64_t range);
void generateBinary2File(uint64_t range);

/****************************************************************************/
/****************-PRIVATE-METHODS-*******************************************/
/****************************************************************************/
/*private method that generates binary*/
static void generateAlphaBinary(uint64_t maxRange,int8_t OUTPUT_ID);

/*private method to get encoding bits representation*/
const int8_t getEncodingBits(uint64_t num);
long double negLog2(uint64_t num);
bool isNumInMaxRange(uint64_t num);

/*private method to write buffer out*/
void write2STDOUT(char *buffer,int bufLen);
void write2FILE(char *buffer,int bufLen,FILE *ptr2File);

/*private method for Clock timer*/
void startTimer(clock_t *start);
double endTimer(clock_t *start);


/**
 * @description - Generates binary upto given range to STDOUT.
 * @param - range in uint64_t format.
 */
void generateBinary(uint64_t range)
{
    generateAlphaBinary(range,OUT_STDOUT);
}

/**
 * @description - Generates binary upto given range to FILE.
 * @param - range in uint64_t format.
 */
void generateBinary2File(uint64_t range)
{
    generateAlphaBinary(range,OUT_FILE);
}




/**
 * @description - 
 * NOTE : This is the main method for generating binary in this libary.
 * Generates all patterns of the binary up to maxlen in length.  This
 * function uses a buffer that holds alphaLen * alphaLen patterns at a time.
 * One pattern of length 5 would be "11111\n".  The reason that alphaLen^2
 * patterns are used is because we prepopulate the buffer with the last 2
 * letters already set to all possible combinations.  So for example,
 * the buffer initially looks like "0000\na0001\n0010\n ... 1111\n".  Then
 * on every iteration, we write() the buffer out, and then increment the
 * third to last letter.  So on the first iteration, the buffer is modified
 * This continues until all combinations of letters are exhausted.
 
 * @param - range in uint64_t format and ID for output.
 */ 
 
static void generateAlphaBinary(uint64_t maxRange,int8_t OUTPUT_ID)
{
	/*Main Alphabet algorithm related variables*/	
	char *binaryAlphabet = "01";
    int8_t maxlen = getEncodingBits(maxRange);
    int   alphaLen = strlen(binaryAlphabet);
    int   len      = 0;
    char *buffer   = malloc((maxlen + 1) * alphaLen * alphaLen);
    int  *letters  = malloc(maxlen * sizeof(int));
	
	/*Binary-Alpha related variables*/
    uint64_t buffer2 = 1,buffer3 = 1;
    uint64_t prevbuffer3 = 1;
    bool firstBool = true,secondBool = true,skipBool = false;
    FILE *ptr2File = NULL;
	char *AlphaBinaryFILE = "ALPHA_BINARY.txt";
	
	/*Timer related variables*/
    clock_t start;
    double timeUsed;
	
	/*Error checking for malloc*/
 	if (buffer == NULL || letters == NULL)
    {
        perror("Error while allocating memory... ");
        exit(EXIT_FAILURE);
    }

    /*If output is selected to FILE*/
    if(OUTPUT_ID == OUT_FILE)
    {

        ptr2File = fopen(AlphaBinaryFILE, "w");

        if (ptr2File == NULL)
        {
            perror("Error with file... ");
            exit(EXIT_FAILURE);
        }

    }
	
    /*Start the clock timer before generating combinations */
    startTimer(&start);

    // This for loop generates all 1 letter patterns, then 2 letters, etc, upto the given maxlen.
    for (len = 1; len <= maxlen; len++)
    {
        // The stride is one larger than len because each line has a '\n'.
        int i;
        int stride = len + 1;
        int bufLen = stride * alphaLen * alphaLen;
        char *ch;
		
		/*We don't need to write here buffer1 is always 0 and we are printing 00 in buffer2 already*/
		if (len == 1) 	    
	    	continue;
	    	
		/*Get decimal equivalent of current buffer*/
	 	buffer2 = strtoull(buffer,&ch,2);
	
		// Initialize buffer to contain all first letters. 
		if(buffer2 != 0)	
		memset(buffer, binaryAlphabet[0], bufLen);

        // Now write all the last 2 letters and newlines, which
        // will after this not change during the main algorithm.
        {
            // Let0 is the 2nd to last letter.  Let1 is the last letter.
            int let0 = 0;
            int let1 = 0;
            for (i = len - 2; i < bufLen; i += stride)
            {
                buffer[i]   = binaryAlphabet[let0];
                buffer[i+1] = binaryAlphabet[let1++];
                buffer[i+2] = '\n';
                if (let1 == alphaLen)
                {
                    let1 = 0;
                    let0++;
                    if (let0 == alphaLen)
                        let0 = 0;
                }
            }
        }
		
		
		// Write the first sequence out.
		       
        /*Special case to write 00 - 11 once*/
        if(buffer2 == 0 && firstBool)
        {
            if(OUTPUT_ID == OUT_STDOUT)            
                write2STDOUT(buffer,bufLen);
            
            else if(OUTPUT_ID == OUT_FILE)
                write2FILE(buffer,bufLen,ptr2File);
            
			/*set to false so that it won't print again causing duplicates*/
            firstBool = false;
        }

        /*Dont write duplicates with leading zero's */
        if(buffer2 != 0)
        {
            if(OUTPUT_ID == OUT_STDOUT)
                write2STDOUT(buffer,bufLen);
            
            else if(OUTPUT_ID == OUT_FILE)
                write2FILE(buffer,bufLen,ptr2File);
        }

        // Special case for length 2, we're already done.
        if (len == 2)
            continue;

        // Set all the letters to 0.
        for (i = 0; i < len;i++)
            letters[i] = 0;

        // Now on each iteration, increment the the third to last letter.
        i = len-3;
        do
        {
            char c;
            int  j;

            // Increment this letter.
            letters[i]++;

            // Handle wraparound.
            if (letters[i] >= alphaLen)
                letters[i] = 0;

            // Set this letter in the proper places in the buffer.
            c = binaryAlphabet[letters[i]];
            
            for (j = i; j < bufLen; j+=stride)
                buffer[j] = c;

            if (letters[i] != 0)
            {
                // No wraparound, so we finally finished incrementing.
                // Write out this set.  Reset i back to third to last letter.
				
				/*Get decimal equivalent of current buffer*/
                buffer3 = strtoull(buffer,&ch,2);

                /*Only set new value to prevbuffer if its's max*/
                if(buffer3 > prevbuffer3)
                    prevbuffer3 = buffer3;

                /*Special case to do/don't skip pattern 0100-1000*/
                if(buffer2 == 0)
                {
					/*Don't skip first pattern for 0100-1000*/
                    if(buffer3 == 4 && secondBool)
                    {
                        secondBool = false;
                        skipBool = false;
                    }
                    
					/*set both conditions to true to skip second pattern 0100-1000*/
                    else if (buffer3 == 4)
                    {
                        buffer3 = 1;
                        skipBool = true;
                    }
					/*set true to skip other numbers*/
                    else
                        skipBool = true;

                    /*Skip all repeated combinations untill max value reached*/
                    if(buffer3 < prevbuffer3 && skipBool)
                        continue;

                }

                /*if buffer3 exhausted max range then break the loop*/
                if(buffer3 >= maxRange)
					break;
                
				// Write the second sequence out.
                if(OUTPUT_ID == OUT_STDOUT)
                    write2STDOUT(buffer,bufLen);

                else if(OUTPUT_ID == OUT_FILE)
                    write2FILE(buffer,bufLen,ptr2File);

                i = len - 3;
                continue;
            }

            // The letter wrapped around ("carried").  Set up to increment
            // the next letter on the left.
            i--;
            // If we carried past last letter, we're done with this
            // whole length.
            if (i < 0)
                break;
        }
        while(true);
    }
	
	/*If we have reached this point it means we have generated all combinations successfully*/
 	timeUsed = endTimer(&start);
    printf("%llu binary numbers generated out of %llu range in %f seconds\n",buffer3 - 1,maxRange,timeUsed);
    
    /*Clean up FILE pointer*/
    if(OUTPUT_ID == OUT_FILE){
    printf("File %s was created successfully \n",AlphaBinaryFILE);
	fclose(ptr2File);
    }
    
	// Clean up.
    free(letters);
    free(buffer);
}

/**
 * @description - Write current buffer to STDOUT
 * @param - buffer and buffer length
 */
 
void write2STDOUT(char *buffer,int bufLen)
{

    char *ERR_MSG = "There was an error while writing output\n";

    if(write(STDOUT_FILENO, buffer, bufLen) != bufLen)
    {
        write(STDERR_FILENO,ERR_MSG,strlen(ERR_MSG));
        exit(EXIT_FAILURE);
    }
}

/**
 * @description - Write current buffer to FILE
 * @param - buffer and buffer length and FILE pointer
 */
 
void write2FILE(char *buffer,int bufLen,FILE *ptr2File)
{

    char *ERR_MSG = "There was an error while writing output\n";

    if(fwrite(buffer,sizeof(uint8_t), bufLen,ptr2File) != bufLen)
    {
        write(STDERR_FILENO,ERR_MSG,strlen(ERR_MSG));
        exit(EXIT_FAILURE);
    }
}

/**
 * @description - Encoding Bits in which provided number could be endcoded
 * @param - decimal number
 * @returns - encoded bits or returns QWORD if bits > 64-bit.
 */
const int8_t getEncodingBits(uint64_t num)
{
	int8_t encodingBits = (int8_t)fabs(floor(negLog2(num) - 1) + 1);
    return  (encodingBits > QWORD) ? QWORD : encodingBits;
}

/**
 * @description - This provides negative Log to the base 2 i.e reciprocal of log2,
 *it uses log2l long double version from math library to implement negative log.
 *Use correct format for long double to avoid precision errors as refrenced.

 * @param - Decimal number (max uint64_t)
 * @returns - negative log to the base 2 in long double format.
 */

long double	negLog2(uint64_t num)
{
    long double negLogVal = 0.0f;
    negLogVal = (num < 0) ? (sizeof(num) * BYTE) : (log2l(1.0L) - log2l(num));
    return isNumInMaxRange(num) ?  fabs(negLogVal) + 1 : negLogVal;
}

/**
 * @description - Checking number for its max range.
 * @param - num to check range.
 * @returns - true if num is in range else returns false.
 */
bool isNumInMaxRange(uint64_t num)
{
    return ((num == (INT8_MAX  + 1U) || (UINT8_MAX  + 1U) ||
             num == (INT16_MAX  + 1U) || (UINT16_MAX  + 1U) ||
             num == (INT32_MAX  + 1ULL) ||(UINT32_MAX  + 1ULL) ))
           ?
           true : false;
}

/**
 * @description - Starts the clock timer
 * @param - start in clock_t format
 */
void startTimer(clock_t *start)
{
    *start = clock();
}

/**
 * @description - Ends the clock timer*
 * @param - reference of start in clock_t format
 * @return - time elapsed in double format.
 */
double endTimer(clock_t *start)
{
    return ((double) (clock() - *start)) / CLOCKS_PER_SEC;
}

#endif /*_ALPHABINARYGENERATOR4C_H_*/