Alpha-Binary generator library is **fastest _sequence-binary_** generator library for C ,and generates all combinations of the binary up to number 'N' efficiently with immense speed.

## Underlying algorithm information.
It uses [Alphabet generation algorithm](https://github.com/Jsdemonsim/Stackoverflow/blob/master/alphabet/alphabet.c) which uses sequence of permutation and combination method to generate all possible combinations of given string.

Time complexity of this algorithm is **_O(n^2)_** , but it still manages to generate binary numbers 
faster than algorithm having time complexity of **_O(n)_**.

## How it achieves 4x times speed.
it achieves 4x times speed than normal binary generator,it uses buffer to store **4-binary numbers** and writes them at once to STDOUT or to FILE,and it uses efficient methods like system call **write()** for STDOUT and **fwrite()** for FILES, thus achieving maximum throughput.

## Output correction : 
Since this algorithm generates binary numbers in buffer which contains binary of 4 numbers at once
so output could be 4x times less than or greater than actual value i.e 'N'.

## Testing guidance. 
The best way to test this program is to output to **/dev/null**, otherwise the file I/O will dominate the test time.
Tested working on **Windows 10** (__64-bit__) and **Linux** (__Ubuntu-17.10-amd64__).

## Alpha-Binary generator vs other sequential binary generator comparison.
[Sequence Binary generator CPP](https://ideone.com/dTJq9y) using inbuilt **Queue** and its time complexity is  **O(n)**

[Sequence Binary generator Java](https://ideone.com/TcJsEO) using inbuilt **Queue** and its time complexity is  **O(n)**

[Sequence Binary generator Python](https://ideone.com/Ku2aBv) using inbuilt **Iterators** and its time complexity is  **O(n)**

[Sequence Binary generator JavaScript](https://ideone.com/UlYWfJ) using **Shift operators** and its time complexity is  **O(n)**

## Speed test comparison graphs.

**__Sequence binary generation comparison Bar-line graph__**
![Bar-Line graph](https://github.com/haseeb-heaven/AlphaBinaryGenerator4c/blob/sources/ALL_BarLine.jpg?raw=true "")

**__Sequence binary generation comparison Area graph__**
![Area Graph](https://github.com/haseeb-heaven/AlphaBinaryGenerator4c/blob/sources/ALL_PERCENT.png?raw=true "")

## Detailed in-depth speed test result for N-Inputs.
[AlphaBinary C generation test result](https://github.com/haseeb-heaven/AlphaBinaryGenerator4c/blob/sources/C_BIN_GEN_SPEED.txt)

[CPP generation speed test reult](https://github.com/haseeb-heaven/AlphaBinaryGenerator4c/blob/sources/CPP_BIN_GEN_SPEED.txt)

[Java generation speed test reult](https://github.com/haseeb-heaven/AlphaBinaryGenerator4c/blob/sources/JAVA_BIN_GEN_SPEED.txt)

[Python generation speed test reult](https://github.com/haseeb-heaven/AlphaBinaryGenerator4c/blob/sources/PY_BIN_GEN_SPEED.txt)

[JavaScript generation speed test reult](https://github.com/haseeb-heaven/AlphaBinaryGenerator4c/blob/sources/JS_BIN_GEN_SPEED.txt)


Written by HaseeB Mir (haseebmir.hm@gmail.com)
Dated : 05/12/2017

