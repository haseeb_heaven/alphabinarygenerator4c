#include "AlphaBinaryGenerator4c.h"

uint64_t atoull(char *str);

int main(int argc, char *argv[])
{
    if (argc < 2) {
	fprintf(stderr, "Usage: %s Length\n", argv[0]);
	exit(EXIT_FAILURE);
    }
	/*Convert input to unsigned long long*/
	uint64_t range = atoull(argv[1]);
    generateBinary(range);
    getchar();
    return 0;
}

// custom ascii to unsigned long long.
uint64_t atoull(char *str)
{
    int res = 0;
 
    // Iterate through all characters of input string and update result
    uint64_t i = 0;
    for (i = 0; str[i] != '\0'; ++i)
        res = res*10 + str[i] - '0';
 
    return res;
}